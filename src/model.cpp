#include <pyur5_pybind/model.h>
#include <string>
#include <iostream>
namespace py = pybind11;

ur5CartPole::FCModel::FCModel(int _train_horizon, int _n_model, int _n_horizon, bool _use_cos) {
    std::cout << "Loading model..." << std::endl;
    calc_ = py::module_::import("pyur5.models.ens_model");
    train_horizon = _train_horizon;
    n_model = _n_model;
    n_horizon = _n_horizon;
    use_cos = _use_cos;
    model_ = calc_.attr("EnsembleModel")();//(train_horizon=train_horizon, n_model=n_model, n_horizon=n_horizon, use_cos=use_cos);//train_horizon, n_model, n_horizon);
}

ur5CartPole::FCModel::~FCModel() {
    model_.release();
    calc_.release();
}

Eigen::VectorXd ur5CartPole::FCModel::predict(const Eigen::VectorXd &obs){
    std::cout << "Predicting..." << std::endl;
    std::cout << "Observation type: " << typeid(obs).name() << std::endl;
    std::cout << "Observation dimensions: " << obs.size() << std::endl;
    py::object action_py = model_.attr("get_sac_action")(obs);
    // std::cout << "Predicted action: " << action_py << std::endl;
    Eigen::VectorXd action = action_py.cast<Eigen::VectorXd>();
    std::cout << "Predicted action: " << action << std::endl;
    return action;
}
std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, double> ur5CartPole::FCModel::forward_traj(const Eigen::VectorXd &obs, const int &n_steps, const std::string &optimizer) {
    py::object action_py = model_.attr("forward_traj")(obs, n_steps, optimizer);
    return action_py.cast<std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, double>>();
}

Eigen::VectorXd ur5CartPole::FCModel::random_action(){
    py::object action_py = model_.attr("random_action")();
    Eigen::VectorXd action = action_py.cast<Eigen::VectorXd>();
    return action;
}

Eigen::MatrixXd ur5CartPole::FCModel::load_trajs(){
    py::object action_py = model_.attr("load_trajs")();
    Eigen::MatrixXd actions = action_py.cast<Eigen::MatrixXd>();
    return actions;
}


