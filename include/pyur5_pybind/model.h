#ifndef MODEL_H
#define MODEL_H

#include <pybind11/eigen.h>  // Import needed to pass Eigen variables to python
#include <pybind11/embed.h>
#include <pybind11/numpy.h>

#include <Eigen/Core>
#include <string>


namespace ur5CartPole {
    namespace py = pybind11;

    class FCModel {
        public:
            FCModel(int _train_horizon, int _n_model, int _n_horizon, bool _use_cos);
            ~FCModel();

            Eigen::VectorXd predict(const Eigen::VectorXd &obs);

            std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, double> forward_traj(const Eigen::VectorXd &obs, const int &n_steps, const std::string &optimizer);

            Eigen::VectorXd random_action();

            Eigen::MatrixXd load_trajs();

        private:
            py::module_ calc_;
            std::string path_;
            py::object model_;
            int train_horizon; // training horizon
            int n_model; // number of models in ensemble
            int n_horizon; // prediction horizon
            bool use_cos;

    };
}
#endif // MODEL_H

